class BottleSong
  def initialize
    @lyrics = '99 bot...'
  end

  def verse(number)
    "#{number} bottles of beer on the wall, #{number} bottles of beer. Take one down and pass it around, #{number - 1} bottles of beer on the wall."
  end

  def lyrics; end
end
